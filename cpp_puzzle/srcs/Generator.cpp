#include <Generator.hpp>

Generator::~Generator() {}

Generator::Generator(Goal &goal): size(goal.size) {
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine gen(seed);
	this->puzzle.resize(goal.goal_lst.size());
	std::copy(goal.goal_lst.begin(), goal.goal_lst.end(), this->puzzle.begin());

	this->iterations = gen() % (2000 / this->size) + 1;
	
	std::cout << "Random start generated in " << std::to_string(iterations) << " swaps" << std::endl;
	this->_do_swaping();
}

Generator::Generator(Goal &goal, unsigned int iterations): size(goal.size), iterations(iterations) {
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine gen(seed);
	this->puzzle.resize(goal.goal_lst.size());
	std::copy(goal.goal_lst.begin(), goal.goal_lst.end(), this->puzzle.begin());

	this->_do_swaping();
}

void	Generator::_do_swaping(void){
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine gen(seed);
	int 								last = -1;
	while (this->iterations > 0){
		std::tuple<int,int> 			zero = this->puzzle[0];
		switch (gen() % 4) {
			case 0 : if (std::get<0>(zero) > 0 && 0 != last){
				this->puzzle[0].swap(*std::find(this->puzzle.begin(), this->puzzle.end(), std::tuple<int,int>(std::get<0>(zero) - 1, std::get<1>(zero))));
				this->iterations--;
				last = 0;
				break ;
			}
			case 1 : if (std::get<0>(zero) < this->size - 1 && 1 != last){
				this->puzzle[0].swap(*std::find(this->puzzle.begin(), this->puzzle.end(), std::tuple<int,int>(std::get<0>(zero) + 1, std::get<1>(zero))));
				this->iterations--;
				last = 1;
				break ;
			}
			case 2 : if (std::get<1>(zero) > 0 && 2 != last){
				this->puzzle[0].swap(*std::find(this->puzzle.begin(), this->puzzle.end(), std::tuple<int,int>(std::get<0>(zero), std::get<1>(zero) - 1)));
				this->iterations--;
				last = 2;
				break ;
			}
			case 3 : if (std::get<1>(zero) < this->size - 1 && 3 != last){
				this->puzzle[0].swap(*std::find(this->puzzle.begin(), this->puzzle.end(), std::tuple<int,int>(std::get<0>(zero), std::get<1>(zero) + 1)));
				this->iterations--;
				last = 3;
				break ;
			}
		}
	}
}

std::ostream &	operator<<(std::ostream & o, Generator const & e){
	for (int y = 0; y < e.size; ++y)
	{
		for (int x = 0; x < e.size; ++x)
		{
			std::cout << std::to_string(e.puzzle.end() - std::find(e.puzzle.begin(), e.puzzle.end(), std::tuple<int,int>(x, y)) - 1);
			std::cout << " ";
		}
		if (y < e.size - 1)
			std::cout << std::endl;
	}
	return o;
}
