#include <Goal.hpp>

Goal::Goal(int s): size(s){
	this->generate_goal();
}

Goal::~Goal(){}

void	Goal::generate_goal(void){
	int					ts = this->size * this->size;
	int					cur = 1;
	int					x = 0;
	int					y = 0;
	int					ix = 1;
	int					iy = 0;
	std::vector<int>	goal;

	goal.resize(ts);
	this->goal_lst.resize(ts);
	for (int i = 0; i < ts; ++i)
		goal[i] = -1;
	while (true) {
		goal[x + y * this->size] = cur;
		this->goal_lst[cur] = std::tuple<int,int>(x, y);
		if (cur == 0)
			break ;
		cur++;
		if (x + ix == this->size\
		 || x + ix < 0\
		 || (ix != 0\
		 && goal[x + ix + y * this->size] != -1)){
			iy = ix;
			ix = 0;
		}
		else if (y + iy == this->size\
		 || y + iy < 0\
		 || (iy != 0\
		 && goal[x + (y + iy) * this->size] != -1)){
			ix = -iy;
			iy = 0;
		}
		x += ix;
		y += iy;
		if (cur == ts)
			cur = 0;
	}
}

bool	Goal::is_solvable(Board &board){
	int		permutations = 0;
	bool	result;
	for (int i = 0; i < this->size * this->size; ++i)
	{
		permutations += abs(std::get<0>(board.coord_lst[i]) - std::get<0>(this->goal_lst[i]));
		permutations += abs(std::get<1>(board.coord_lst[i]) - std::get<1>(this->goal_lst[i]));
	}
	if (this->size % 2)
		result = (!(permutations % 2));
	else {
		if (abs(this->size - std::get<1>(board.coord_lst[0])) % 2)
			result = (!(permutations % 2));
		else
			result = (permutations % 2);
	}
	return (result);
}

bool	Goal::is_solution(Board &board){
	return (this->goal_lst == board.coord_lst);
}
