#include "Parser.hpp"
using namespace std;

Parser::Parser(std::vector<std::string> &puzzle){
	this->_puzzle.resize(puzzle.size());
	copy(puzzle.begin(), puzzle.end(), this->_puzzle.begin());
	removeComments();
    checkLexis();
    checkSyntax();
    createCoordLst();
}

Parser::~Parser(){
}

// Lexer	&				Lexer::operator=(const Lexer & rhs){
// 	if (DEBUGG == 1)
// 		std::cout << "Lexer operator =  called" << std::endl;
// 	this->_lst = rhs.getList();
// 	this->_tokenList = rhs.getTokensList();
// 	this->_error = rhs.getError();
// 	return *this;
// }

void	Parser::removeComments(){
	vector<string> puzzleCommentFree; 
	regex reg("(?=#).*$"); 
	for (vector<string>::const_iterator it = this->_puzzle.begin(); it != this->_puzzle.end(); ++it){
    		
		puzzleCommentFree.push_back(regex_replace(*it, reg, "", 
		 							regex_constants::match_any));
		// cout << *it << endl;	
	}
	// cout << endl;
	this->_puzzle = puzzleCommentFree;
}


vector<string> Parser::split(string str){

    istringstream buf(str);
    istream_iterator<string> beg(buf), end;
    vector<string> tokens(beg, end); // done!

    return tokens;
}

// trim from start (in place)
inline void Parser::ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
}

// trim from end (in place)
inline void Parser::rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
}

// trim from both ends (in place)
inline void Parser::trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}


void	Parser::checkLexis(){
	vector<vector<int>> tmp_puzzle;
	for (size_t i = 0; i < _puzzle.size(); i++){
    	trim(_puzzle[i]);
        if ( !_puzzle[i].empty() && _puzzle[i] != "\n"){
    		vector<string> lst_nbr;
            // cout <<"_puzzle[i]" <<_puzzle[i] << endl;
    		lst_nbr = split(_puzzle[i]);

    		vector<int> tmp_int_lst;
    		for (size_t nbr = 0; nbr < lst_nbr.size(); nbr++){
                // cout <<"lst_nbr[nbr]" << lst_nbr[nbr] << endl;
                for (size_t dig = 0; dig < lst_nbr[nbr].length(); dig++){
        			if (!std::isdigit(lst_nbr[nbr][dig])){
                        throw std::logic_error("ParserError: Lexis error: '" + lst_nbr[nbr] + "' is not a positive number");
        			}
                }
                tmp_int_lst.push_back(stoi(lst_nbr[nbr]));
            }
            // cout<< "fin infos" << endl;
    		tmp_puzzle.push_back(tmp_int_lst);
    	}	
		// cout << *it << endl;	
	   }
    	this->_coord = tmp_puzzle;
        // for (size_t n = 0; n < _coord.size(); n++){
        //     for (size_t a = 0; a  < _coord[n].size(); a++)
        //         cout << std::to_string(_coord[n][a]) <<" ";
        //     cout << endl;
        // }
	// for (vector<string>::const_iterator it = this->_puzzle.begin(); it != this->_puzzle.end(); ++it){
    		
	// 	puzzleCommentFree.push_back(regex_replace(*it, reg, "", 
	// 	 							regex_constants::match_any));
	// 	// cout << *it << endl;	
	// }
}

void	Parser::checkSyntax(){
    if (isPuzzleSizeOk()){
        // cout << "isPuzzleSizeOk : true" << endl;
        if (isPuzzleStructOk()){
            // cout << "isPuzzleStructOk : true" << endl;
            if (isPuzzleTilesOk()){
                // cout << "isPuzzleTilesOk : true" << endl;
                return ;
            }
        }
    }
    cout << "end of checkSyntax" << endl;
}

bool	Parser::isPuzzleSizeOk(){
    if (_coord[0].size() > 1){
        throw logic_error("ParserError: Syntax error: puzzle size should be "
                            "define by a single number.");
    }else{
        // cout << _coord[0][0] << endl;
        int x = _coord[0][0];
        if (x <= 1){
            throw logic_error("ParserError: Syntax error: Minimum puzzle size"
                                " is 2");
        }else if (x >= 10){
            cout << "Hmm, You can probably go grap a coffee somewhere...\n";
            cout << "Results will be very long to calculate. We strongly " ;
            cout << "recommend that you hard stop this program otherwise " ;
            cout << "your correction time will be over before we could " ;
            cout << "solve that one..." << endl;
        }
        this->size = static_cast<size_t>(x);
    }
    return true;
}

bool	Parser::isPuzzleStructOk(){
    if (_coord.size() ==  this->size + 1){
        for(size_t i = 1; i < this->size + 1; i++){
            if (_coord[i].size() != this->size){
                throw logic_error("ParserError: Syntax error: the row nbr " +
                                    to_string(i) + " do not have the number of"
                                    " elements specified as the N-puzzle size.");
            }
        }
        return true;
    }
    else{
        throw logic_error("ParserError: Syntax error: the number of row does"
                            " not match the N-puzzle size specified.");
    }
    return false;
}

bool	Parser::isPuzzleTilesOk(){
    vector<int> sample;
    vector<int> copy_coord;

    for (size_t i = 0; i < this->size * this->size; i++){
        sample.push_back(i);
    }

    for (size_t nbr = 0; nbr < _coord.size(); nbr++){
        for (size_t dig = 0; dig < _coord[nbr].size(); dig++){
            copy_coord.push_back(_coord[nbr][dig]);
        }
    }
    copy_coord.erase(copy_coord.begin());
    sort(copy_coord.begin(), copy_coord.end());
    // cout << to_string(copy_coord.size()) << " " <<  to_string(sample.size()) << endl;
    // cout << "copy_coord" << endl;
    //     for (size_t i = 0; i < copy_coord.size(); i++){
    //        cout << copy_coord[i] << endl;
    //     }
    // cout << "sample" << endl;
    // for (size_t i = 0; i < sample.size(); i++){
    //        cout << sample[i] << endl;
    //     }

    if (copy_coord.size() == sample.size()){
        for (size_t i = 0; i < copy_coord.size(); i++){
            if (copy_coord[i] != sample[i]){
                throw logic_error("ParserError: Syntax error: tiles number are wrong."
                                    " It could be a bad range of number or a number"
                                    " represented twice.");
            }
        }
    }else{
        throw logic_error("ParserError: Syntax error: tiles number are wrong."
                            " It could be a bad range of number or a number"
                            " represented twice.");
    }

    return true;
}

void	Parser::createCoordLst(){
    _coord.erase(_coord.begin());
    for (size_t i = 0; i < this->size * this->size; i++){
        this->coord_lst.push_back(tuple<int, int>(-1, -1));
    }

    for(size_t y =0 ; y < this->size; y++){
        for (size_t x = 0; x < this->size; x++){
            this->coord_lst[_coord[y][x]] = tuple<int,int>(x,y);
        // cout << "("<< to_string(x);
        // cout << ", " << to_string(y) << ")"<< endl;

        }
    }
    // cout << "___________"<< endl;
    // for (size_t i = 0 ;  i < coord_lst.size(); i++){
    //     cout << "coord_lst[" << to_string(i) << "]: ("<<to_string(get<0>(coord_lst[i]));
    //     cout << ", " << to_string(get<1>(coord_lst[i])) << ")"<< endl;

    // }
}