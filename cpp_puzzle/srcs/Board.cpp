#include <Board.hpp>

Board::Board(): size(0), depth(0){
}

Board::Board(std::vector<std::tuple<int,int> > & coord_lst, int size, int depth, std::string parent): coord_lst(coord_lst), size(size), depth(depth), parent_hash(parent){
	this->cost = 0;
	for (int i = 0; i < size * size; ++i)
	{
		unsigned long x = std::get<0>(coord_lst[i]);
		unsigned long y = std::get<1>(coord_lst[i]);
		this->hash.append(std::to_string(x));
		this->hash.append(std::to_string(y));
	}
}
Board::Board(std::vector<std::tuple<int,int> > & coord_lst, int size, int depth): coord_lst(coord_lst), size(size), depth(depth){
	this->cost = 0;
	for (int i = 0; i < size * size; ++i)
	{
		unsigned long x = std::get<0>(coord_lst[i]);
		unsigned long y = std::get<1>(coord_lst[i]);
		this->hash.append(std::to_string(x));
		this->hash.append(std::to_string(y));
	}
}

Board::Board(const Board & rhs): coord_lst(rhs.coord_lst), size(rhs.size), cost(rhs.cost), depth(rhs.depth), parent_hash(rhs.parent_hash), hash(rhs.hash) {}

Board::~Board(){}

Board								&Board::operator=(const Board & r){
	this->coord_lst = r.coord_lst;
	this->size = r.size;
	this->cost = r.cost;
	this->depth = r.depth;
	this->hash = r.hash;
	this->parent_hash = r.parent_hash;
	return (*this);
}

std::string		Board::get_hash(void){
	return (hash);
}


std::vector<Board>		*Board::get_sons(void){
	std::vector<Board> *sons = new std::vector<Board>();
	int x = std::get<0>(coord_lst[0]);
	int y = std::get<1>(coord_lst[0]);
	for (std::vector<std::tuple<int,int> >::iterator elem = coord_lst.begin(); elem != coord_lst.end(); ++elem)
	{
		if (x > 0 && std::get<0>(*elem) == x - 1 && std::get<1>(*elem) == y){
			std::vector<std::tuple<int,int> > l;
			l = std::vector<std::tuple<int,int> >(coord_lst);
			l[0].swap(l[elem - coord_lst.begin()]);
			sons->emplace_back(l, this->size, this->depth + 1, this->hash);
		} else if (x < this->size - 1 && std::get<0>(*elem) == x + 1 && std::get<1>(*elem) == y){
			std::vector<std::tuple<int,int> > l;
			l = std::vector<std::tuple<int,int> >(coord_lst);
			l[0].swap(l[elem - coord_lst.begin()]);
			sons->emplace_back(l, this->size, this->depth + 1, this->hash);
		} else if (y > 0 && std::get<1>(*elem) == y - 1 && std::get<0>(*elem) == x){
			std::vector<std::tuple<int,int> > l;
			l = std::vector<std::tuple<int,int> >(coord_lst);
			l[0].swap(l[elem - coord_lst.begin()]);
			sons->emplace_back(l, this->size, this->depth + 1, this->hash);
		} else if (y < this->size - 1 && std::get<1>(*elem) == y + 1 && std::get<0>(*elem) == x){
			std::vector<std::tuple<int,int> > l;
			l = std::vector<std::tuple<int,int> >(coord_lst);
			l[0].swap(l[elem - coord_lst.begin()]);
			sons->emplace_back(l, this->size, this->depth + 1, this->hash);
		}
	}
	return (sons);
}

bool		Board::operator<(const Board & r) const{
	return this->cost + this->depth < r.cost + r.depth;
}

bool		Board::operator>(const Board & r) const{
	return this->cost + this->depth > r.cost + r.depth;
}

bool		Board::operator>=(const Board & r) const{
	return this->cost + this->depth >= r.cost + r.depth;
}

bool		Board::operator==(const Board & r) const{
	return this->hash == r.hash;
}

std::ostream &	operator<<(std::ostream & o, Board const & e){
	// o << "hash: " << e.hash << std::endl;
	int		width = std::to_string(e.size * e.size).size() + 1;
	std::cout.fill(' ');
	for (int y = 0; y < e.size; ++y)
	{
		for (int x = 0; x < e.size; ++x)
		{
			for (int i = 0; i < e.size * e.size; ++i)
			{
				if (std::get<0>(e.coord_lst[i]) == x && std::get<1>(e.coord_lst[i]) == y){
					o << std::setw(width) << std::left << i;
				}
			}
		}
		if (y == 0)
			o << "\tcost: " << std::to_string(e.cost);
		else if (y == 1)
			o << "\tdepth: " << std::to_string(e.depth);
		// if (y == 2)
		// 	o << "\tparent: " << e.parent_hash;
		if (y < e.size - 1)
			o << std::endl;
	}
	return o;
}