#include "Heuristics.hpp"
using namespace std;

Heuristics::Heuristics(int n): nbr(n){

}

Heuristics::~Heuristics(){

}

bool		Heuristics::is_same_row(Board &node, Goal &goal, int val){
	if (get<0>(node.coord_lst[val]) == get<0>(goal.goal_lst[val])){
		return true;
	}
	return false;
}
bool		Heuristics::is_same_col(Board &node, Goal &goal, int val){
	if (get<1>(node.coord_lst[val]) == get<1>(goal.goal_lst[val])){
		return true;
	}
	return false;
}

int			Heuristics::djikistra(Board &node, Goal &goal, int size){
	static_cast<void>(node);
	static_cast<void>(goal);
	static_cast<void>(size);
	return 1;
}

int			Heuristics::manhattan(Board &node, Goal &goal, int size){
	int		permutations = 0;
	for (int i = 1; i < size * size; ++i)
	{
		permutations += abs(get<0>(node.coord_lst[i]) - get<0>(goal.goal_lst[i]));
		permutations += abs(get<1>(node.coord_lst[i]) - get<1>(goal.goal_lst[i]));
	}
	// cout << "permutations:" << to_string(permutations) << endl;
	return permutations;
}

int			Heuristics::tiles_out_of_row_and_column(Board &node, Goal &goal, int size){
	int out = 0;
	for(int val = size * size; val-- ;){
		if (! is_same_row(node, goal, val))
			out += 1;
		if (! is_same_col(node, goal, val))
			out +=1;
	}
	// cout << "out:" << to_string(out) << endl;
	return out;
}

int			Heuristics::misplaced_tiles(Board &node, Goal &goal, int size){
	int misplaced = 0;
	for (int val = 1; val < size * size; val++){
		if (!is_same_row(node, goal, val) or !is_same_col(node, goal, val)){
			misplaced += 1;
		}
	}
	// cout << "misplaced:" << to_string(misplaced) << endl;
	return misplaced;
}


int			Heuristics::find_val(vector<tuple<int,int> > &goal_lst, tuple<int,int> zero, int size){
	for (int i = size * size; i-- ; ){
		if (get<0>(goal_lst[i]) == get<0>(zero) && get<1>(goal_lst[i]) == get<1>(zero)){
			return i;
		}
	}
	return -1;
}

int		Heuristics::search_misplaced(vector<tuple<int,int> > &cp_node_coord_lst, vector<tuple<int,int> > &goal_lst, int size){
	for (int val = 1; val < size * size; val++){
		if ((get<0>(cp_node_coord_lst[val]) != get<0>(goal_lst[val])) || (get<1>(cp_node_coord_lst[val]) != get<1>(goal_lst[val])))
			return val;
	}
	return (-1);
}

void		Heuristics::print_list(vector<tuple<int,int> > &e, int size){
	for (int y = 0; y < size; ++y)
	{
		for (int x = 0; x < size; ++x)
		{
			for (int i = 0; i < static_cast<int>(e.size()); ++i)
			{
				if (get<0>(e[i]) == x && get<1>(e[i]) == y){
					cout << i << " ";
				}
			}
		}
		if (y < static_cast<int>(e.size()) - 1)
			cout << endl;
	}

}

int			Heuristics::gaschnig(Board &node, Goal &goal, int size){
	int swaps = 0;
	vector<tuple<int,int> > cp_node_coord_lst = node.coord_lst;
	
	while (issolution(cp_node_coord_lst, goal.goal_lst, size) == false){
		int val = find_val(goal.goal_lst, cp_node_coord_lst[0], size);
		if (val == 0){
			val = search_misplaced(cp_node_coord_lst, goal.goal_lst, size);
		}
		if (val > 0){
			std::swap(cp_node_coord_lst[0], cp_node_coord_lst[val]);	
			swaps +=1;
		}		
	}
	// cout << "swaps:" << to_string(swaps) << endl;
	return swaps;
}


bool		Heuristics::issolution(vector<tuple<int,int> > &cp_node_coord_lst, vector<tuple<int,int> > &goal_coord_lst, int size){
	static_cast<void>(size);
	return (cp_node_coord_lst == goal_coord_lst);
}

int 		Heuristics::add_linear_conflicts(vector<vector<vector<int> > > conflics_lst){
    int permutations = 0;

    for (int i = static_cast<int>(conflics_lst.size()); i--; )
    {
    	vector<vector<int> > cp_lst;
    	cp_lst = conflics_lst[i];
    	for (int j = static_cast<int>(conflics_lst[i].size()); j--; )
    	{
    		for (int k = static_cast<int>(cp_lst.size()); k--; )
	    	{	

	    		if (cp_lst[k][0] != conflics_lst[i][j][0]){

	    			if (cp_lst[k][0] < conflics_lst[i][j][0] && cp_lst[k][1] > conflics_lst[i][j][1]){
	    				permutations += 2;
	    			}
	    			// else if (cp_lst[k][0] > conflics_lst[i][j][0] && cp_lst[k][1] < conflics_lst[i][j][1]){
	    			// cout << "perm2:" << endl;
	    			// 	permutations += 2;
	    			// }
	    		}
	    	}
	    	if (cp_lst.size() != 0){
	    		cp_lst.pop_back();
	    	}	
    	}
    }

    return permutations;
}

void		Heuristics::print_conflict(vector<vector<vector<int> > > vect_x){
	for (size_t i = 0; i < vect_x.size(); ++i)
	{
		for (size_t j = 0; j < vect_x[i].size(); ++j)
		{
			for (size_t k = 0; k < vect_x[i][j].size(); ++k)
			{
				cout <<"vect[" <<to_string(i)<<"][" <<to_string(j) <<"][" <<to_string(k) << "]"  <<to_string(vect_x[i][j][k])<< ", ";		
			}
			cout << endl;
		}
	}

}


int 		Heuristics::linear_conflict(Board &node, Goal &goal, int size){
    int permutations = 0;
    vector<vector<vector<int> > > vect_x;
    vector<vector<vector<int> > > vect_y;
    
	for(int i = 0; i < size ; i++)
	{
		vector<vector<int> > tmp;
		vect_x.emplace_back(tmp);
		vect_y.emplace_back(tmp);
	} 
	for (int val = 1; val < size * size; val++)
	{
		if (is_same_row(node, goal, val) && !is_same_col(node, goal, val)){
			vector<int> tmp;
			
			tmp.emplace_back(get<1>(node.coord_lst[val]));
			
			tmp.emplace_back(get<1>(goal.goal_lst[val]));
			vect_x[get<0>(node.coord_lst[val])].emplace_back(tmp);
		}
		else if(is_same_col(node, goal, val) && !is_same_row(node, goal, val)){
			vector<int> tmp_bis;
			tmp_bis.emplace_back(get<0>(node.coord_lst[val]));
			tmp_bis.emplace_back(get<0>(goal.goal_lst[val]));
			vect_y[get<1>(node.coord_lst[val])].emplace_back(tmp_bis);
		}
		permutations += abs(get<0>(node.coord_lst[val]) - get<0>(goal.goal_lst[val]));
		permutations += abs(get<1>(node.coord_lst[val]) - get<1>(goal.goal_lst[val]));
	}

	permutations += add_linear_conflicts(vect_x);
	permutations += add_linear_conflicts(vect_y);	

	// cout << "permutations:" << to_string(permutations) << endl; 
    return permutations;
}


int			Heuristics::distance(Board &node, Goal &goal, int size){
 
 	map<int, int (Heuristics::*)(Board&, Goal&, int )> heuristics_vect;

	heuristics_vect[0] = &Heuristics::djikistra;  
	heuristics_vect[1] = &Heuristics::manhattan;
	heuristics_vect[2] = &Heuristics::tiles_out_of_row_and_column;
	heuristics_vect[3] = &Heuristics::misplaced_tiles;
	heuristics_vect[4] = &Heuristics::gaschnig;
	heuristics_vect[5] = &Heuristics::linear_conflict;
// (this->*(map[inst->getCommandEnum()]))(*inst)
	return (this->*(heuristics_vect[this->nbr]))(node, goal, size);
}