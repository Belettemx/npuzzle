#include <Astar.hpp>

Astar::Astar(Heuristics &heuristic): _heuristic(heuristic), _max_open_lst(0), len_open_closed(0) {

}

Astar::~Astar(){}

bool							Astar::_is_in_open_lst(Board &son){
	CustomPriorityQueue<Board>::t_const_iterator b = this->_open_lst.find(son);
	// bool						result = false;
	// t_priority_queue			q;
	if(!this->_open_lst.is_last(b) && son >= *b){
		// this->_open_lst.remove(b);
		return (true);
	}
	// while (!this->_open_lst.empty()){
	// 	Board b = this->_open_lst.top();
	// 	this->_open_lst.pop();
	// 	if (son.hash == b.hash && son >= b)
	// 		result = true;
	// 	else
	// 		q.emplace(b);
	// }
	// this->_open_lst = q;
	// std::cout << "in open list: " << std::to_string(result) << std::endl;
	return (false);
}

bool							Astar::_is_in_closed_lst(Board &son){
	Board						b;
	if (!this->_closed_lst.empty()\
		&& this->_closed_lst.find(son.hash) != this->_closed_lst.end()){
		b = this->_closed_lst[son.hash];
		// std::cout << "------------- closed list: -------------" << std::endl;
		// std::cout << b << std::endl;
		// std::cout << "----------------------------------------" << std::endl;
	} else
		return (false);
	return (son >= b);
}

Board							Astar::_solution_path(Board node){
	if (!node.parent_hash.empty()){
		this->_solution_path(this->_closed_lst[node.parent_hash]);
		std::cout << "++++++++++++++++++++++++++++++" << std::endl;
		std::cout << node << std::endl;
	} else {
		std::cout << "Complexity in time: " << std::to_string(this->_max_open_lst) << std::endl;
		std::cout << "Complexity in size: " << std::to_string(this->len_open_closed) << std::endl;
	}
	return (node);
}

void							Astar::calculate(Goal &goal, int size, Board &start_board){
	start_board.cost = this->_heuristic.distance(start_board, goal, size);
	this->_open_lst.push(start_board);
	std::cout << "############## Start Board #############" << std::endl;
	std::cout << start_board << std::endl;
	std::cout << "########################################" << std::endl;
	while (!this->_open_lst.empty()){
		Board elem = this->_open_lst.top();
		this->_open_lst.pop();
		// std::cout << "**************"/*" Looking at **************"*/ << std::endl;
		// std::cout << elem << std::endl;
		if (goal.is_solution(elem)){
			// std::cout << "******************************" << std::endl;
			std::cout << "Moves needed to solve the puzzle: " << elem.depth << std::endl;
			this->_solution_path(elem);
			return ;
		}
		// std::cout << "========================================" << std::endl;
		// this->_closed_lst[elem.hash] = elem;
		std::vector<Board> *sons = elem.get_sons();
		for (std::vector<Board>::iterator son = sons->begin(); son != sons->end(); ++son)
		{
			son->cost = this->_heuristic.distance(*son, goal, size);
			// std::cout << *son << std::endl;
			if (!this->_is_in_closed_lst(*son) && !this->_is_in_open_lst(*son)){
				this->_open_lst.push(*son);
				// std::cout << "=== PUSHED ===";
			}
			// std::cout << std::endl;
		}
		this->_closed_lst[elem.hash] = elem;

		
		// std::cout << "len open_lst: " << this->_open_lst.size() << std::endl;
		// std::cout << "len closed_lst: " << this->_closed_lst.size() << std::endl;
		if (this->_open_lst.size() > this->_max_open_lst){
			this->_max_open_lst = this->_open_lst.size();
		}
		if (this->_max_open_lst + this->_closed_lst.size() > this->len_open_closed){
			this->len_open_closed = this->_max_open_lst + this->_closed_lst.size();
		}
	}
	std::cout << "Complexity in time: " << std::to_string(this->_max_open_lst) << std::endl;
	std::cout << "Complexity in size: " << std::to_string(this->len_open_closed) << std::endl;
	return ;
}


// int heuri(Board board, Goal goal, int size){

// 	int		permutations = 0;
// 	for (int i = 0; i < goal.size * goal.size; ++i)
// 	{
// 		permutations += abs(std::get<0>(board.coord_lst[i]) - std::get<0>(goal.goal_lst[i]));
// 		permutations += abs(std::get<1>(board.coord_lst[i]) - std::get<1>(goal.goal_lst[i]));
// 	}
// 	return (permutations);
// }



// int main(int argc, char const *argv[])
// {
// 	std::vector<std::tuple<int,int> > lst;
// 	lst.push_back(std::tuple<int, int>(2,0));
// 	lst.push_back(std::tuple<int, int>(2,2));
// 	lst.push_back(std::tuple<int, int>(0,0));
// 	lst.push_back(std::tuple<int, int>(0,1));
// 	lst.push_back(std::tuple<int, int>(1,1));
// 	lst.push_back(std::tuple<int, int>(2,1));
// 	lst.push_back(std::tuple<int, int>(0,2));
// 	lst.push_back(std::tuple<int, int>(1,0));
// 	lst.push_back(std::tuple<int, int>(1,2));
// 	Board board = Board(lst, 3, 0, NULL);


// 	Goal goal = Goal(3);
// 	Astar astar = Astar(&heuri);
// 	astar.calculate(goal, 3, board);
// 	return 0;
// }