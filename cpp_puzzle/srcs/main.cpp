#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <tclap/CmdLine.h>
#include "Parser.hpp"
#include "Goal.hpp"
#include "Board.hpp"
#include "Heuristics.hpp"
#include "Generator.hpp"
#include "Astar.hpp"

#define NBR_HEURISTIC 5

int main(int argc, char** argv)
{

	// Wrap everything in a try block.  Do this every time, 
	// because exceptions will be thrown for problems.
	try {  

	// Define the command line object, and insert a message
	// that describes the program. 
	TCLAP::CmdLine cmd("Command description message", ' ', "0.1");

	// puzzle argument
	TCLAP::ValueArg<std::string> puzzleArg(
											"p",
											"puzzle",
											"A file containing the N-puzzle to"
											"solve.", 
											false,
											"", 
											"string");

	// Add the argument nameArg to the CmdLine object. The CmdLine object
	// uses this Arg to parse the command line.
	cmd.add( puzzleArg );



	// heuristic argument (with allowed values)
	std::vector<int> heuristicsAllowed;

	// allowed values based on NBR_HEURISTIC present into the program
	for (int i = 0; i <= NBR_HEURISTIC; i++){
		heuristicsAllowed.push_back(i);
	}
	TCLAP::ValuesConstraint<int> allowedHeuristics(heuristicsAllowed);
		
	TCLAP::ValueArg<int> heuristicArg(
										"e", 
										"heuristic", 
										"The name of the heuristic which "
										"should be use to solve the N-puzzle."
										"\n0: Djikistra\n1: Manhattan distance"
										"\n2: tiles out of row and column\n"
										"3: Misplaced tiles"
										"\n4: N-Max swaps (Gaschnig)",
										false, 
										-1, 
										&allowedHeuristics);
	cmd.add( heuristicArg );


	// Size argument
	TCLAP::ValueArg<int> sizeArg(
								"s",
								"size",
								"The size of the N-puzzle > 1.",
								false,
								-1, 
								"int");
	cmd.add( sizeArg );

	TCLAP::ValueArg<int> swapsArg(
								"w",
								"swaps",
								"(optionnal) Number of swaps to generate the "
								"N-puzzle. If the number given is equal or "
								"inferrio to zero, then this option will be "
								"ignore.",
								false,
								0,
								"int");
	cmd.add( swapsArg );

	// Define a switch and add it to the command line.
	// A switch arg is a boolean argument and only defines a flag that
	// indicates true or false.

	// greedy, uniform and default argumments
	TCLAP::SwitchArg greedySwitch(
									"g",
									"greedy",
									"The N-puzzle will be solved with a greedy"
									" algorithm.", 
									cmd, 
									false);

	
	TCLAP::SwitchArg uniformSwitch(
									"u",
									"uniform_cost",
									"The N-puzzle will be solved with a "
									"uniform cost algorithm.",
									cmd,
									false);

	TCLAP::SwitchArg defaultSwitch(
									"d",
									"default",
									"Launch the program with following options"
									":\n - 3*3 puzzle generated automatically "
									"or specified size with -s\n - greedy "
									"algorithm except if specified otherwise\n"
									" - Manhattan distance heuristic except if"
									" specified otherwise.", 
									cmd, 
									false);

	// Parse the argv array.
	cmd.parse( argc, argv );


	// Get the value parsed by each arg.
	 
	std::string	fileName	= puzzleArg.getValue();
	int 		heuristic	= heuristicArg.getValue();
	int 		size		= sizeArg.getValue();
	int 		swaps 		= swapsArg.getValue();
	bool		greedy		= greedySwitch.getValue();
	bool		uniform		= uniformSwitch.getValue();
	bool		def			= defaultSwitch.getValue();


	// std::cout << "greedy: "<< greedy << std::endl;
	// std::cout << "uniform: "<< greedy << std::endl;
	// std::cout << "default: "<< def << std::endl;
	// std::cout << "size: "<< size << std::endl;
	// std::cout << "heuristic: "<< heuristic << std::endl;
	// std::cout << "fileName: "<< fileName << std::endl;
	


	// check arg and open IO for puzzle if needed + ask questions if needed
	if (greedy && uniform){
		throw std::logic_error("The algorithm can not be uniform cost and "
								"greedy.");  
	}

	if (heuristic > -1 && uniform){
		throw std::logic_error("Uniform cost algorithm can not be mix with an" 
								"heuristic.");  
	}

	if (size < 0 && def){
		if (size < 0){
			size = 3;
		}
		if (!greedy && !uniform){
			greedy = true;
		}
		if (heuristic < 0){
			heuristic = 1;
		}
	}

	if (size < 2){
		std::string tmp;
		while (size < 2){
			std::cout << "Please, enter a n-puzzle size:" << std::endl;
			std::getline(std::cin, tmp);
			size = std::stoi(tmp);
		}

		if (size > 10){
			std::cout << "Hmm, You can probably go grab a coffee somewhere..."
			"\nResults will be very long to calculate. We strongly recommend "
			"that you hard stop this program otherwise your correction time "
			"will be over before we could solve that one..." << std::endl;
		}
	}

	if ((!uniform || greedy) && (heuristic < 0 || heuristic > 5)){
		std::string tmp;
		while (heuristic < 0 || heuristic > 5){
			std::cout << "Please, enter an heuristic number:" << std::endl;
			std::getline(std::cin, tmp);
			heuristic = std::stoi(tmp);
		}
	}

	if (swaps < 0){
		swaps = 0;
	}

	Parser *parser = NULL;
	Generator *gen = NULL;
	Goal goal(size);
	Board start_board;
	// std::cout << fileName << std::endl;
	if (!fileName.empty()){
		std::fstream file(fileName, std::ios::in);

		if (file)
		{
			std::vector<std::string> puzzle;
			std::string line;
			while (getline(file, line))
			{
				puzzle.push_back(line);
			}
			file.close();

			// mettre parser avec le puzzle passé en params
			// for (std::vector<std::string>::const_iterator i = puzzle.begin(); i != puzzle.end(); ++i){
			// 	std::cout << *i << std::endl;
			// }
			parser = new Parser(puzzle);
			start_board = Board(parser->coord_lst, size, 0);

		}
		else
			throw std::logic_error("Could not open the file:" + fileName);
	}else{
		// mettre parser avec generateur
		if (swaps != 0){
			// std::cout <<"swaps gen" << std::endl;
			gen = new Generator(goal, swaps);
		}
		else
			gen = new Generator(goal);
		start_board = Board(gen->puzzle, size, 0);
	}
	// std::cout << start_board << std::endl;
	if (goal.is_solvable(start_board)){
		Heuristics he = Heuristics(heuristic);
		// std::cout << std::to_string(he.linear_conflict(start_board, goal, size)) << std::endl;
		Astar astar = Astar(he);
		astar.calculate(goal, size, start_board);
	} else {
		std::cout << "Not solvable" << std::endl;
	}


	} catch (TCLAP::ArgException &e)  // catch any exceptions
	{ 
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
	} catch ( const std::exception & e ) 
	{ 
		std::cerr << e.what(); 
	} 
}