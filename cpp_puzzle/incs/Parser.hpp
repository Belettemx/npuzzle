#ifndef PARSER_HPP
#define PARSER_HPP

#include <string>
#include <vector>
#include <tuple>
#include <iostream>
#include <regex>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <string>
#include <sstream>
#include <iterator>
#include <ctype.h>



class Parser{
	private:
		std::vector<std::string>			_puzzle;
		std::vector<std::vector<int>> 		_coord;

	public:

		size_t									size;
		std::vector<std::tuple<int,int> >	coord_lst;
									Parser(std::vector<std::string> &puzzle);
		virtual						~Parser();
	 	void						removeComments();
	 	void						checkLexis();
	 	void						checkSyntax();
	 	bool						isPuzzleSizeOk();
	 	bool						isPuzzleStructOk();
	 	bool						isPuzzleTilesOk();
	 	void						createCoordLst();
		static inline void 			ltrim(std::string &s);
		static inline void 			rtrim(std::string &s);
		static inline void 			trim(std::string &s);
		std::vector<std::string>	split(std::string str);
};

#endif