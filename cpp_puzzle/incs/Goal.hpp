#ifndef GOAL_HPP
#define GOAL_HPP

#include <tuple>
#include <vector>
#include <iostream>
#include <math.h>
#include <Board.hpp>

class Goal
{
private:
	void								generate_goal(void);

public:
	int									size;
	std::vector<std::tuple<int, int> >	goal_lst;

										Goal(int size);
										~Goal();
	bool								is_solvable(Board &board);
	bool								is_solution(Board &board);
	
};

#endif