#ifndef BOARD_HPP
#define BOARD_HPP

#include <iostream>
#include <iomanip>
#include <vector>
// #include <map>
#include <tuple>

class Board
{
public:
	std::vector<std::tuple<int,int> >	coord_lst;
	int									size;
	int									cost;
	int									depth;
	std::string							parent_hash;
	std::string							hash;

	std::string							get_hash(void);
	std::vector<Board>					*get_sons(void);
										Board(std::vector<std::tuple<int,int> >\
											&coord_lst, int size, int depth,\
											std::string parent);
										Board(std::vector<std::tuple<int,int> > & coord_lst, int size, int depth);
										Board(const Board & rhs);
										Board();
										~Board();
	Board								&operator=(const Board & r);
	bool								operator<(const Board & r) const;
	bool								operator>(const Board & r) const;
	bool								operator>=(const Board & r) const;
	bool								operator==(const Board & r) const;
};

std::ostream &	operator<<(std::ostream & o, Board const & e);
#endif	