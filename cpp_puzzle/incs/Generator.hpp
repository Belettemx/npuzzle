#ifndef GENERATOR_HPP
# define GENERATOR_HPP

#include <tuple>
#include <vector>
#include <algorithm>
#include <random>
#include <iostream>
#include <Goal.hpp>
#include <Board.hpp>

class Generator
{
private:
	void			_do_swaping(void);
public:
	std::vector<std::tuple<int,int> >		puzzle;
	int										size;
	unsigned int 							iterations;

	Generator(Goal &goal);
	Generator(Goal &goal, unsigned int iterations);
	~Generator();
};

std::ostream &	operator<<(std::ostream & o, Generator const & e);
#endif