#ifndef ASTAR_HPP
#define ASTAR_HPP

#include <iostream>
#include <queue>
#include <unordered_map>
#include <Board.hpp>
#include <Goal.hpp>
#include <Heuristics.hpp>
#include <algorithm>
#include <array>
#include <vector>
#include <Custom_priority_queue.hpp>


class Astar
{
private:
	CustomPriorityQueue<Board>				_open_lst;
	std::unordered_map<std::string,Board>	_closed_lst;
	Heuristics								&_heuristic;

	bool				_is_in_open_lst(Board &son);
	bool				_is_in_closed_lst(Board &son);
	Board				_solution_path(Board node);

public:
	unsigned long		_max_open_lst;
	unsigned long		len_open_closed;

	Astar(Heuristics &heuristic);
	~Astar();

	void				calculate(Goal &goal, int size, Board &start_board);
};

#endif