#ifndef HEURISTICS_HPP
#define HEURISTICS_HPP

#include <string>
#include <vector>
#include <tuple>
#include <iostream>
#include <regex>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <string>
#include <sstream>
#include <iterator>
#include <map>
#include <ctype.h>
#include "Board.hpp"
#include "Goal.hpp"


class Heuristics{

	private:
		bool	is_same_row(Board &node, Goal &goal, int val);
		bool	is_same_col(Board &node, Goal &goal, int val);
		int		search_misplaced(std::vector<std::tuple<int,int> > &cp_node_coord_lst, std::vector<std::tuple<int,int> > &goal_coord_lst, int size);
		bool	issolution(std::vector<std::tuple<int,int> > &cp_node_coord_lst, std::vector<std::tuple<int,int> > &goal_coord_lst, int size);
		int		find_val(std::vector<std::tuple<int,int> > &goal_lst, std::tuple<int,int> zero, int size);
		void	print_list(std::vector<std::tuple<int,int> > &coord, int size);
		int 	add_linear_conflicts(std::vector<std::vector<std::vector<int> > > conflics_lst);
	    void	print_conflict(std::vector<std::vector<std::vector<int> > > vect_x);

	public:

		int 						nbr;
									Heuristics(int n);
		virtual						~Heuristics();
		int							djikistra(Board &node, Goal &goal, int size);

		int							manhattan(Board &node, Goal &goal, int size);
		int							tiles_out_of_row_and_column(Board &node, Goal &goal, int size);
		int							misplaced_tiles(Board &node, Goal &goal, int size);
		int							gaschnig(Board &node, Goal &goal, int size);
		int							distance(Board &node, Goal &goal, int size);
		int 						linear_conflict(Board &node, Goal &goal, int size);
};

#endif