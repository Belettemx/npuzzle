#ifndef CUSTOM_PRIORITY_QUEUE_HPP
#define CUSTOM_PRIORITY_QUEUE_HPP

#include <algorithm>
#include <queue>

template<class T, class Container = std::vector<T>, class Compare = std::greater<typename Container::value_type> >
class CustomPriorityQueue: public std::priority_queue<T, Container, Compare>
{
public:
	typedef typename std::priority_queue<T, Container, Compare>::container_type::const_iterator t_const_iterator;

	// We may try to search for the forth sons in one loop
	// it should be more optimized :
	// 	if sons are in a priority list as well
	// 	we should look for the first one and then pop it(or go to next)
	// 	because the open list should be ordered
	//		 and we look from the smallest to the greaest
	t_const_iterator find(const T&val) const
	{
		auto first = this->c.begin();
		auto last = this->c.end();
		while (first != last){
			if (*first == val){
				return (first);
			}
			++first;
		}
		return (last);
	}

	bool is_last(const t_const_iterator &val) const
	{
		return (val == this->c.end());
	}

	bool remove(t_const_iterator & val) {
		if (val != this->c.end()) {
			this->c.erase(val);
			std::make_heap(this->c.begin(), this->c.end(), this->comp);
			return (true);
		} else {
			return (false);
		}
	}
};

#endif