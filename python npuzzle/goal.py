
# The formula says:
# If the grid width is odd:
#        then the number of inversions in a solvable situation is even.
# If the grid width is even
# and the blank is on an even row counting from the bottom
# (second-last, fourth-last etc):
#        then the number of inversions in a solvable situation is odd.
# If the grid width is even
# and the blank is on an odd row counting from the bottom
# (last, third-last, fifth-last etc):
#        then the number of inversions in a solvable situation is even.
# That gives us this formula for determining solvability:

# ( (grid width odd) && (#inversions even) )
# ||
# ((grid width even) && ((blank on odd row from bottom) && (#inversions even)))


class Goal(object):
    """
    docstring for Goal
    """
    def __init__(self, size: int):
        super(Goal, self).__init__()
        self.size = size
        self.goal_dic = dict()
        self._generate_goal()
        self.goal_coord_lst = self._get_coord_lst()

    def _generate_goal(self):
        ts = self.size * self.size
        goal = [-1 for i in range(self.size * self.size)]
        cur = 1
        x = 0
        ix = 1
        y = 0
        iy = 0
        while True:
            goal[x + y * self.size] = cur
            self.goal_dic[(x, y)] = cur
            if cur == 0:
                break
            cur += 1
            if x + ix == self.size or x + ix < 0 or (ix != 0 and goal[x + ix + y * self.size] != -1):
                iy = ix
                ix = 0
            elif y + iy == self.size or y + iy < 0 or (iy != 0 and goal[x + (y + iy) * self.size] != -1):
                ix = -iy
                iy = 0
            x += ix
            y += iy
            if cur == ts:
                cur = 0

    def issolvable(self, board):
        permutations = 0
        zero = board.coord_lst[0]
        for val in range(0, self.size * self.size):
            permutations += abs(board.coord_lst[val][0] - self.goal_coord_lst[val][0]) 
            permutations += abs(board.coord_lst[val][1] - self.goal_coord_lst[val][1])

        if self.size % 2:

            return bool(not(permutations % 2))
        else:
            # verifier que cette partie est correcte, j'ai changer zero[2] par zero[1].

            if abs(self.size - zero[1]) % 2:
                return bool(not (permutations % 2))
            else:
                return bool(permutations % 2)

    def _get_coord_lst(self):
        tmp_dic = dict()
        lst_coord = list()
        # print(self.goal_dic)
        for key, val in self.goal_dic.items():
            # print("tmp_dic[{}] = {}".format(val, key))
            tmp_dic[val] = key

        for val in range(0, self.size * self.size):
            lst_coord.append(tmp_dic[val])

        return lst_coord

    def issolution(self, board):
        """
        Check if the board pass in params is the n-puzzle solved.
        If it's the case, True is return, otherwise, False is return.
        """
        # for val in range(self.size * self.size):
            # if board.coord_lst[val] != self.goal_coord_lst[val]:
                # return False
        # return True
        return board.coord_lst == self.goal_coord_lst