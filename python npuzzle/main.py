#!/usr/bin/python3
# -*- coding: utf-8 -*-
import argparse
import textwrap
from parser import Parser
from generator import Generator
from board import Board
from goal import Goal
from astar import AStar
from idastar import IDAStar
import heuristics

NBR_HEURISTIC = 5


def size_type(x):
    """
    Check either the N-puzzle size specified by the user is over 1 or not.
    If it's over 10. A message is display to alert the user about time
    consumption of his correction time allowance.
    """
    x = int(x)
    if x <= 1:
        raise argparse.ArgumentTypeError("Minimum size is 2")
    elif x >= 10:
        print('Hmm, You can probably go grab a coffee somewhere...\nResults will'
              ' be very long to calculate. We strongly recommend that you hard stop'
              ' this program otherwise your correction time will be over before we'
              ' could solve that one...')
    return x


def heuristic_type(x):
    """
    Check either heuristic argument given by the user is hold between 1 and
    NBR_HEURISTIC. NBR_HEURISTIC, represent the number of heuristics implemented.
    """
    x = int(x)
    if x < 0 or x > NBR_HEURISTIC:
        raise argparse.ArgumentTypeError('Heuristic should be a number between '
                                         '1 and ' + str(NBR_HEURISTIC) +
                                         ' (included).')
    return x


def check_args(args):
    """
    Check args and ask the user for some help if some datas are missing.
    """
    # if uniform_cost and heuristic options are set, an exception is raised.
    if args.uniform_cost and args.heuristic:
        raise argparse.ArgumentTypeError('Uniform cost algorithm can not be mix'
                                         ' with an heuristic.')
    # check if default option is set, and adapt default set with other options
    if args.default:
        if not args.size:
            args.size = 3
        if not args.greedy and not args.uniform_cost and not args.idastar:
            args.greedy = True
        if not args.heuristic:
            args.heuristic = 1

    # check if the user set all the option needed. If he didn't, the program ask
    # fora size or an heuristic or both. the program check as well user input
    # are correct
    if not args.size:
        args.size = size_type(input("Please, enter a n-puzzle size:"))
    if (not args.uniform_cost  or args.greedy ) and not args.idastar and not args.heuristic:
        args.heuristic = heuristic_type(input("Please, enter an heuristic number:"))


def main(args):
    """
    main function.
    """
    check_args(args)

    # parse puzzle (given by user or automaticly generated)
    if args.puzzle:
        parser = Parser(args.puzzle.readlines())
    else:
        parser = Parser(Generator(args.size).lst)
    start_board = Board(coord_lst=parser.get_coord_lst(), size=parser.get_size(), depth=0)
    # print(start_board.hash)
    # print(start_board)
    # print("### sons ###")
    # lst = start_board.get_sons()
    # for l in lst:
    #     print(l)
    goal = Goal(parser.get_size())
    if goal.issolvable(start_board):
        # print(goal.issolution(start_board))
        # print(goal.issolution(goal.goal_coord_lst))
    
    # # start_board.hash()
    # # sons = start_board.get_sons()
    # # for so in sons:
    # #     print(so)
    # # check if puzzle solvable
    # # launch algo with the puzzle
    # # print results
        print("##### START BOARD #######")
        print(start_board)
        if args.idastar:
            algo = IDAStar()
            solution = algo.iterative_deepening_search(start_board, goal, 90, parser.get_size())
            print("Moves needed to solve the puzzle: {}".format(solution[0].depth))
            
           
            for elem in reversed(solution):
                print(elem)
        else:
            algo = AStar(start_board, heuristics.heuristic_dic[args.heuristic])
            solution = algo.caculate(goal, parser.get_size(), start_board)
        if not solution:
            print("No solution found!")
    else:
        print("Not solvable.")

if __name__ == "__main__":
    '''
    Definition of the argument parser. Arguments are pass to main function.
    '''
    parser = argparse.ArgumentParser(
        description='Resolve N-puzzle.',
        formatter_class=argparse.RawTextHelpFormatter)

    # Create a mutually exclusive group. argparse will make sure that only one
    # of the arguments in the mutually exclusive group was present on the
    # command line.
    group = parser.add_mutually_exclusive_group()

    # --greedy and --uniform_cost can not be set at the same time
    group.add_argument('-g',
                       '--greedy',
                       action='store_const',
                       const=True,
                       help='The N-puzzle will be solved with a greedy'
                            ' algorithm.')

    group.add_argument('-u',
                       '--uniform_cost',
                       action='store_const',
                       const=True,
                       help='The N-puzzle will be solved with a uniform cost'
                            ' algorithm.')

    # other arguments
    parser.add_argument('-s',
                        '--size',
                        type=size_type,
                        help='The size of the N-puzzle > 1.')

    parser.add_argument('-p',
                        '--puzzle',
                        type=argparse.FileType('r'),
                        help='A file containing the N-puzzle to solve.')

    parser.add_argument('-he',
                        '--heuristic',
                        type=heuristic_type,
                        help=textwrap.dedent(
                            'The name of the heuristic which should be use to'
                            ' solve the N-puzzle.\n1: Manhattan distance\n'
                            '2: Linear conflict\n3: N-Max swaps (Gaschnig)'
                            '\n4: tiles out of row and column\n5: Misplaced '
                            'tiles'))

    parser.add_argument('-d',
                        '--default',
                        action='store_const',
                        const=True,
                        help=textwrap.dedent(
                            'Launch the program with following options:\n - 3*3 '
                            'puzzle generated automatically or specified size with -s'
                            '\n - greedy algorithm except if specified otherwise\n'
                            ' - Manhattan distance heuristic except if specified'
                            ' otherwise.'))
    parser.add_argument('-ida',
                        '--idastar',
                        action='store_const',
                        const=True,
                        help=textwrap.dedent(
                            'Launch the program with following IDA star '
                            'algorithm instead of A star algorithm'))

    try:
        args = parser.parse_args()
        main(args)
    except Exception as e:
         print(e)
         parser.print_usage()
