#!/usr/bin/python3
# -*- coding: utf-8 -*-
import heapq
import queue
import math
import time


class AStar(object):

    def __init__(self, start_board, heuristic):
        self.closed_lst = dict()
        # self.open_lst = [(math.nan, start_board)]
        # heapq.heapify(self.open_lst)
        self.open_lst = queue.PriorityQueue(0)
        self.max_open_lst = 0
        self.len_open_closed = 0
        self.heuristic = heuristic

    def caculate(self, goal, size, start_board):
        steps = 0
        start_board.cost = self.heuristic(start_board, goal, size)
        self.open_lst.put_nowait((start_board.cost, start_board))
        while not self.open_lst.empty():
            # start = time.clock()
            # elem = heapq.heappop(self.open_lst)
            elem = self.open_lst.get_nowait()
            # if elem[1].depth > 10:
            #     raise Exception("on a dit stop!!!")
            if goal.issolution(elem[1]):
                print("Moves needed to solve the puzzle: {}".format(elem[1].depth))
                return self.solution_path(elem[1])
            # print("============================================")
            # print(elem[1])
            # print("--------------------------------------------")
            for son in elem[1].get_sons(size):
                son.cost = self.heuristic(son, goal, size) #+ elem[1].cost
                if not self.is_in_closed_lst(son) and not self.is_in_open_lst(son):
                    # print("cost updated")
                    # heapq.heappush(self.open_lst, (son.cost, son))
                    self.open_lst.put_nowait((son.cost, son))
                # print("cost: {}, depth:{}".format(son.cost, son.depth))
            

                # print(son)
            self.closed_lst[elem[1].hash] = elem[1]
            # print("moves:")
            # print(elem[1])
            # print("len open_lst: {}".format(self.open_lst.qsize()))
            # print("len closed_lst: {}".format(len(self.closed_lst)))
            if self.open_lst.qsize() > self.max_open_lst:
                self.max_open_lst = self.open_lst.qsize()
            if self.open_lst.qsize() + len(self.closed_lst) > self.len_open_closed:
                self.len_open_closed = self.open_lst.qsize() + len(self.closed_lst)
            # steps += 1
            # print("step ({}): {}".format(steps, time.clock() - start))
        return None


    def is_in_open_lst(self, son):
        # print("=============================")
        q = queue.PriorityQueue(0)
        result = False
        while not self.open_lst.empty():
            elem = self.open_lst.get_nowait()
            if son.hash == elem[1].hash and son.cost >= elem[1].cost :
                # print("is_in_open_lst: True")
                # del (self.open_lst[son.hash])
                # heapq.heapify(self.open_lst)
                result = True
            else:
                q.put_nowait(elem)
        self.open_lst = q
        # print("is_in_open_lst: False")
        return result

    def is_in_closed_lst(self, son):
        # print("is_in_closed_lst operations")
        elem = self.closed_lst.get(son.hash, None)
        if elem:
            if son.cost >= elem.cost:
                # print("is_in_closed_lst: True")
                return True
        # print("is_in_closed_lst: False")
        return False

    def solution_path(self, node):

        if node and node.parent_hash :
            self.solution_path(self.closed_lst[node.parent_hash])
            print(node)
        else:
            print("Complexity in time: {}".format(self.max_open_lst))
            print("Complexity in size: {}".format(self.len_open_closed))

        return node