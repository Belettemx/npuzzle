from goal import Goal 

class IDA(object):
    CUTOFF = -1


    def __init__(self, root, goal, heuristic):
        self.root = root
        self.goal = goal 
        self.heuristic = heuristic

    def calculate(self):
        limit = heuristic(self.root)
        while (1)
        {
            result = search(self.root, 0, limit)
            if result == CUTOFF:
                return None

            if self.goal.issolution(result):
                return result

        }


    root=initial node;
    Goal=final node;
    function IDA*()                                             //Driver function
    {

        threshold=heuristic(Start);
        while(1)             //run for infinity
        {

            integer temp=search(Start,0,threshold); //function search(node,g score,threshold)
            if(temp==FOUND)                                 //if goal found
                     return FOUND;                                             
            if(temp== ∞)                               //Threshold larger than maximum possible f value
                     return;                               //or set Time limit exceeded
            threshold=temp;

        }

    }


    function Search(node, g, threshold)              //recursive function
    {

    f=g+heuristic(node);
    if(f>threshold)             //greater f encountered
             return f;
    if(node==Goal)               //Goal node found
             return FOUND;
    integer min=MAX_INT;     //min= Minimum integer
    foreach(tempnode in nextnodes(node))
    {

    //recursive call with next node as current node for depth search
    integer temp=search(tempnode,g+cost(node,tempnode),threshold);  
    if(temp==FOUND)            //if goal found
    return FOUND;
    if(temp<min)     //find the minimum of all ‘f’ greater than threshold encountered                                min=temp;

    }
    return min;  //return the minimum ‘f’ encountered greater than threshold

    }
    function nextnodes(node)
    {
                 return list of all possible next nodes from node;
    }

