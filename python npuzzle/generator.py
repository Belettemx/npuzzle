import random
import argparse

class Generator:
    """docstring for Generator"""
    def __init__(self, width):
        self.width = width
        self.puzzle = [i for i in range(width*width)]
        random.seed()
        random.shuffle(self.puzzle)
        w = len(str(self.width * self.width))
        lst = list(str(self.width))
        for y in range(self.width):
            res = str()
            for x in range(self.width):
                res += "{} ".format(str(self.puzzle[x + y*self.width]).rjust(w))
            lst.append(res)
        self.lst = lst

    def __str__(self):
        res = str()
        for s in self.lst:
            res += str(s)
            if(self.lst.index(s) < len(self.lst) - 1):
                res += "\n"
        return res


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("size", type=int, help="Size of the puzzle's side. Must be >3.")

    args = parser.parse_args()

    p = Generator(args.size)
    print(p.lst)
