#!/usr/bin/python3
# -*- coding: utf-8 -*-
from goal import Goal
from board import Board


################# commomns functions used by differents heuristics ############

def is_same_row(node, goal, val):
    if goal.goal_coord_lst[val][0] == node.coord_lst[val][0]:
        return True
    return False

def is_same_col(node, goal, val):
    if goal.goal_coord_lst[val][1] == node.coord_lst[val][1]:
        return True
    return False

################# uniform_cost heuristic ####################

def djikistra(node, goal, size):
    return 1

################# manhattan heuristic ####################

def manhattan(node, goal, size):
    """The manhattan distance heuristic."""
    permutations = 0
    for val in range(1, size * size):
        permutations += abs(goal.goal_coord_lst[val][0] - node.coord_lst[val][0]) 
        permutations += abs(goal.goal_coord_lst[val][1] - node.coord_lst[val][1])
    # print("manhattan distance: {}".format(permutations))

    # print("permutations: " + str(permutations))

    return permutations

################# linear_conflict heuristic ####################

def add_linear_conflicts(conflics_dic):
    permutations = 0
    for key, lst in conflics_dic.items():
        cp_lst = lst
        for i in range(len(lst) - 1, -1, -1):
            for j in range(len(lst) - 1, -1, -1):
                if lst[i] != cp_lst[j]:
                    if lst[i][0] < cp_lst[j][0] and lst[i][1] > cp_lst[j][1]:
                        # print("add perm 1: {}, {}".format(lst[i], cp_lst[j]))
                        permutations += 2
                    elif lst[i][0] > cp_lst[j][0] and lst[i][1] < cp_lst[j][1]:
                        # print("add perm 2: {}, {}".format(lst[i], cp_lst[j]))
                        permutations += 2
            if lst:
                del lst[i]
    return permutations

def linear_conflict(node, goal, size):
    """The linear conflic heuristic."""
    permutations = 0
    dic_x = dict()
    dic_y = dict()
    for val in range(0, size):
        dic_x[val] = list()
        dic_y[val] = list()
    for val in range(1, size * size):
        if is_same_row(node, goal, val) and not is_same_col(node, goal, val):
            # print("x val: {}, node({}), goal({})".format(val, node.coord_lst[val], goal.goal_coord_lst[val]))
            dic_x[node.coord_lst[val][0]].append((node.coord_lst[val], goal.goal_coord_lst[val]))
            
        elif is_same_col(node, goal, val) and not is_same_row(node, goal, val):
            # print("y val: {}, node({}), goal({})".format(val, node.coord_lst[val], goal.goal_coord_lst[val]))
            dic_y[node.coord_lst[val][1]].append((node.coord_lst[val], goal.goal_coord_lst[val]))

        permutations += abs(goal.goal_coord_lst[val][0] - node.coord_lst[val][0])
        permutations += abs(goal.goal_coord_lst[val][1] - node.coord_lst[val][1])


    # print("dic_y:")
    # print(dic_y)
    permutations += add_linear_conflicts(dic_y)

    # print("dic_x:")
    # print(dic_x)
    permutations += add_linear_conflicts(dic_x)

    # print("manhattan distance: {}".format(permutations))
    # print("permutations: " + str(permutations))
    return permutations

################# tiles_out_of_row_and_column heuristic ####################

def tiles_out_of_row_and_column(node, goal, size):
    out = 0
    for val in range(0, size * size):
        if not is_same_row(node, goal, val):
            out += 1
        if not is_same_col(node, goal, val):
            out += 1

    # print("out: " + str(out))
    return out

################# misplaced_tiles heuristic ####################

def misplaced_tiles(node, goal, size):
    misplaced = 0
    for val in range(1, size * size):
        if node.coord_lst[val] != goal.goal_coord_lst[val]:
            misplaced += 1
    # print("misplaced: " + str(misplaced))
    return misplaced


################# gaschnig heuristic ####################


def search_misplaced(cp_node_coord_lst, goal_coord_lst, size):
    # print("search_misplaced")
    # print_coord_lst(cp_node_coord_lst, size)
    # print_coord_lst(goal_coord_lst, size)
    for val in range(1, size * size):
        if cp_node_coord_lst[val] != goal_coord_lst[val]:
            # print("ret val:" + str(val))
            return val
    # print("ret -1")
    return -1

def issolution(cp_node_coord_lst, goal_coord_lst, size):
    return cp_node_coord_lst == goal_coord_lst


# def print_coord_lst(cp_node_coord_lst, size):
#     tmp_dic = dict()

#     for val in range(size * size):
#         tmp_dic[cp_node_coord_lst[val]] = val

#     # create the string that will be print 
#     row = str()
#     for y in range(size):
#         for x in range(size):
#             row += str(tmp_dic[(x, y)]) + " "
#         row += '\n'
#     print(row)

def gaschnig(node, goal, size):
    """
    The number of steps it would take to solve the problem if it was possible
    to swap any tile with the "space".
    """
    swaps = 0
    cp_node_coord_lst = list(node.coord_lst)
    while not issolution(cp_node_coord_lst, goal.goal_coord_lst, size):        
        # get value of the tiles which should be at the 0 tiles place
        val = goal.goal_dic[cp_node_coord_lst[0]]
        if val == 0:
            val = search_misplaced(cp_node_coord_lst, goal.goal_coord_lst, size)        
        if val > 0:
            # swap 0 and the tiles that should be at the 0 place. If 0 is at
            # right place, then, it's swap with the first misplaced tile 
            # previously found
            cp_node_coord_lst[0], cp_node_coord_lst[val] = \
                cp_node_coord_lst[val], cp_node_coord_lst[0]
            swaps += 1
    # print("swaps: " + str(swaps))
    return swaps

################# heuristic functions dictionnary ####################

heuristic_dic = {
            0 : djikistra,
            1 : manhattan,
            2 : linear_conflict,
            3 : gaschnig,
            4 : tiles_out_of_row_and_column,
            5 : misplaced_tiles,
        }
