#!/usr/bin/python3
# -*- coding: utf-8 -*-


class Board(object):

    def __init__(self, coord_lst, size, depth, parent_hash=None):
        self.coord_lst = coord_lst
        self.parent_hash = parent_hash
        self.hash = self.get_hash(size)
        self.depth = depth
        self.cost = 0
        

    def __str__(self):
        info = "cost: {}, depth: {}\n".format(self.cost, self.depth)
        row = info

        # transform self.coord_lst into a dict[(x, y)] = value of tiles
        size = int(len(self.coord_lst) ** 0.5)
        tmp_dic = dict()
        for val in range(size * size):
            tmp_dic[self.coord_lst[val]] = val

        # create the string that will be print 
        for y in range(size):
            for x in range(size):
                row += str(tmp_dic[(x, y)]) + " "
            row += '\n'
        return row

    def get_hash(self, size):
        '''
        transform the board tuple into an hash.
        '''
        s = str()
        for val in range(0, size * size):
            s += str(self.coord_lst[val][0]) + str(self.coord_lst[val][1])
        return s
        
    def _swap(self, elem1, elem2):
        lst = list(self.coord_lst)
        lst[elem1], lst[elem2] = lst[elem2], lst[elem1]
        return lst

    def get_sons(self, size):
        # get coord of zero tile
        lst_sons = []
        x_zero = self.coord_lst[0][0]
        y_zero = self.coord_lst[0][1]
        COORD_MAX = size
        # print("COORD_MAX: {}, 0({}, {})".format(COORD_MAX, x_zero, y_zero))
        for val in range(0, size * size):
            # print("{}({},{})".format(val, self.coord_lst[val][0], self.coord_lst[val][1]))
            if x_zero - 1 >= 0 and self.coord_lst[val][0] == x_zero - 1 and self.coord_lst[val][1] == y_zero:
                lst = self._swap(0, val) 
                lst_sons.append(Board(coord_lst=lst, size=size, parent_hash=self.hash, depth=self.depth + 1))
            
            if x_zero + 1 < COORD_MAX and self.coord_lst[val][0] == x_zero + 1 and self.coord_lst[val][1] == y_zero:
                lst = self._swap(0, val) 
                lst_sons.append(Board(coord_lst=lst, size=size, parent_hash=self.hash, depth=self.depth + 1))
            
            if y_zero - 1 >= 0 and self.coord_lst[val][0] == x_zero and self.coord_lst[val][1] == y_zero - 1:
                lst = self._swap(0, val) 
                lst_sons.append(Board(coord_lst=lst, size=size, parent_hash=self.hash, depth=self.depth + 1))
            
            if y_zero + 1 < COORD_MAX and self.coord_lst[val][0] == x_zero and self.coord_lst[val][1] == y_zero + 1:
                lst = self._swap(0, val) 
                lst_sons.append(Board(coord_lst=lst, size=size, parent_hash=self.hash, depth=self.depth + 1))
        
        return lst_sons
        # # # pour x - 1
        # # if coord_zero[0] > 0:
        # #     lst = self._swap(coord_zero, (coord_zero[0] - 1, coord_zero[1]))
            
        
        # # pour x + 1
        # if coord_zero[0] < self.size -1:
        #     lst = self._swap(coord_zero, (coord_zero[0] + 1, coord_zero[1]))
        #     lst_sons.append(Board(coord_lst=lst, size=self.size, parent_hash=self.hash, depth=self.depth + 1))
                    
        # # pour y - 1
        # if coord_zero[1] > 0:
        #     lst = self._swap(coord_zero, (coord_zero[0], coord_zero[1] -1))
        #     lst_sons.append(Board(coord_lst=lst, size=self.size, parent_hash=self.hash, depth=self.depth + 1))

        # # pour y + 1
        # if coord_zero[1] < self.size -1:
        #     lst = self._swap(coord_zero, (coord_zero[0], coord_zero[1] + 1))
        #     lst_sons.append(Board(coord_lst=lst, size=self.size, parent_hash=self.hash, depth=self.depth + 1))



    def __lt__(self, other):
        return self.cost < other.cost