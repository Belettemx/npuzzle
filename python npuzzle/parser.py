#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re
# from typing import List


class Parser(object):
    def __init__(self, puzzle):
        """
        puzzle has to be a list
        """
        self.puzzle = puzzle
        self.size = 0
        self.coord_lst = list()
        self._remove_comments()
        self._check_lexis()
        self._check_syntax()
        self._create_lst_coord()

    def _remove_comments(self):
        """
        Remove comments from the puzzle
        """
        re_comment = re.compile(r'(?=#).+$\n')
        puzzle_comment_free = []
        for line in self.puzzle:
            puzzle_comment_free.append(re_comment.sub("", line))
        self.puzzle = puzzle_comment_free

    # print(self.puzzle)

    def _check_lexis(self):
        tmp_puzzle = []
        # print(self.puzzle)
        for line in self.puzzle:

            if line.strip() and line != '\n':
                # split line and check each number to see if they contain only
                # digit. Check digit and not number to raise an error in case of
                # negative number
                lst_num = line.split()
                tmp_int_lst = []
                for number in lst_num:
                    if not number.isdigit():
                        raise Exception('ParserError: Lexis error: "' + number + '" is not a positive number')
                    else:
                        tmp_int_lst.append(int(number))
                tmp_puzzle.append(tmp_int_lst)
        self.puzzle = tmp_puzzle

    def _check_syntax(self):
        """
        Check syntax of n-puzzle given. If an error is detected in a subfunction
        an Exception will be raise.
        """
        # print("before check size")
        if self._is_puzzle_size_ok():
            # print("before check struct")
            if self._is_puzzle_struct_ok():
                # print("before check tiles")
                if self._is_puzzle_tiles_ok():
                    return None  # replace by a superb creation of a board object

    def _is_puzzle_size_ok(self):
        """
        Check if the N-puzzle size specified by the user contain only one number
        and either this number i is over 1 or not.
        If it's over 10. A message is display to alert the user about time
        consumption of his correction time allowance.
        In the same time, self.size is provide size is ok.
        """
        if len(self.puzzle[0]) > 1:
            raise Exception('ParserError: Syntax error: puzzle size should be '
                            'define by a single number.')
        else:
            x = int(self.puzzle[0][0])
            if x <= 1:
                raise Exception('ParserError: Syntax error: Minimum puzzle size'
                                ' is 2')
            elif x >= 10:
                print('Hmm, You can probably go grap a coffee somewhere...\n'
                      'Results will be very long to calculate. We strongly '
                      'recommend that you hard stop this program otherwise '
                      'your correction time will be over before we could '
                      'solve that one...')
            self.size = x
            return True

    def _is_puzzle_struct_ok(self):
        """
        Check either each row of the N-puzzle given get the right number of
        element, given the puzzle size provide.
        """
        if len(self.puzzle) == self.size + 1:
            for i in range(1, self.size + 1):
                if len(self.puzzle[i]) != self.size:
                    raise Exception('ParserError: Syntax error: the row nbr ' +
                                    str(i) + ' do not have the number of'
                                             ' elements specified as the N-puzzle size.')
            return True
        else:
            raise Exception('ParserError: Syntax error: the number of row does'
                            ' not match the N-puzzle size specified.')

    def _is_puzzle_tiles_ok(self):
        """
        Check if tiles number are unique and are between 0 and 9size * size -1).
        """
        sample = set(range(0, self.size * self.size))
        puzzle_tiles = list()
        for i in range(1, self.size + 1):
            puzzle_tiles = puzzle_tiles + self.puzzle[i]

        if set(puzzle_tiles) != sample:
            raise Exception('ParserError: Syntax error: tiles number are wrong.'
                            ' It could be a bad range of number or a number'
                            ' represented twice.')

    def _create_lst_coord(self):
        """
        Transform the puzzle list of list of int into a list of tuple(id, x, y).
        id : is the number representing a specific tile
        x,y : represent coordinates of the tile into the board
        """
        # here it's size+1 because self.puzzle[0] contain the size of the puzzle
        # Tile a
        self.puzzle.pop(0)
        coord_dic = dict()
        #put into a dic[val] = coord
        for y in range(0, self.size):
            for x in range(0, self.size):
                # print("{} : ({}, {})".format(self.puzzle[y][x], x, y))
                coord_dic[str(self.puzzle[y][x])] = (x, y)
        # transform the dict into a list where each index match the
        # corresponding value. lst[0] = coord (x, y) => lst at index 0
        # stock coord of value 0.
        for val in range(0, self.size * self.size):
            self.coord_lst.append(coord_dic[str(val)])  

        # print(self.coord_lst)

    def get_coord_lst(self):
        return self.coord_lst

    def get_size(self):
        return self.size
