#!/usr/bin/python3
# -*- coding: utf-8 -*-
from goal import Goal

class IDAStar(object):
    CUTOFF = -1
    FAILURE = -2
    closed_lst = list()
    def iterative_deepening_search(self, start_board, goal, max_depth, size):

        for depth in range(max_depth):
            # print("#### depth: " + str(depth) + "#####")
            result = self.search(start_board, goal, depth, size)
            # print("ret search dans IDA search:")
            # print(result)
            if result != self.CUTOFF :
                # print("dans IDA search, result:")
                # print(result)
                
                self.closed_lst.append(start_board)
                print("Complexity in time: {}".format(depth))
                print("Complexity in size: {}".format(depth + len(self.closed_lst)))
                return self.closed_lst # end of the IDA
            # clear contenant result
            self.closed_lst.clear()
        return self.CUTOFF

    def search(self, node, goal, depth_limit, size):
        # print("beginning search funct")
        if goal.issolution(node):
            # print("issolution: TRUE")
            return node
        if depth_limit == 0:
            # print("deph_limit == 0")
            return self.CUTOFF # signifie cutoff  car deph_limit reached.

        # print("for son in get_sons:")
        for son in node.get_sons(size):
            # print("son:")
            # print(son)
            result = self.search(son, goal, depth_limit -1, size)
            # print("ret search dans search result:")
            # print(result)
            if result == self.CUTOFF:
                # print("result == cutoff")
                cutoffOccurred = True
            elif result != self.FAILURE:
                # print("dans search, result:")
                # print(result)
                self.closed_lst.append(son)
                return result
        if cutoffOccurred:
            # print("return cutoff")
            return self.CUTOFF
        else:
            # print("return failure")
            return self.FAILURE